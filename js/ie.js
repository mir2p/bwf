$(document).ready( function() {
	
	$('img').each(function() {
		var src = $(this).attr('src');
		$(this).attr('src', src.replace(/\.svg$/i, '.png'));
    });
});